# Docker Compose example

## Description

Customer provided Django blog app.

The task was to write a Docker Compose file with 3x services: 

- Nginx
- Postgresql
- Django app and all dependencies.


The solution was to use Nginx as proxy for Django blog.  User connects to Nginx , Nginx acts as proxy to Django and Django connects to postgresql.
Access to Django was done via Gunicorn.

